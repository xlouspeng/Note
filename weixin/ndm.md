免费且轻巧多线程下载工具：Neat Download Manager，支持win + mac双平台
=================

说道优秀的下载软件，我们不得不提及经典的迅雷，曾经的迅雷支持ed2k、bt、http、ftp等多种下载链接格式，同时还有千万加速节点，奈何现在各种提示“应版权方要求，文件无法下载”，亡矣。

迅雷去了，还有非常受欢迎的下载神器Internet Download Manager，IDM下载拥有的多线程下载、智能切割技术让其声名鹊起，一跃成为了全网有志之士最为钟爱的下载神器。

IDM是一款优秀的付费下载器，单台设备1年的授权官网售价76.5元人民币，这是非常不符合看这篇文章的白嫖党原则的。IDM今年年初开始疯狂刷版本号，经常跳出破解是把你的弹窗，让萝卜哥非常头疼。

![alt 001](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/001.png)

今天，萝卜哥就为小伙伴们介绍一下IDM的“孪生兄弟”：Neat Download Manager。

NDM是一款免费的多线程下载工具，可以通过调整下载的最大连接数为32线程。软件体积非常小仅850KB。软件特点是体积小巧，功能强大，支持在windows和mac os操作系统上安装使用。NDM在功能和界面上和IDM非常相似，相较而言NDM更加简洁易用，可惜NDM暂时没有中文，不用担心萝卜哥来搞定。

 ![alt 002](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/002.png)
 ![alt 003](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/003.png) 
 
 
NDM的外观和IDM非常相似，同样拥有简洁直观的UI界面。NDM的界面更加简洁易用，从左到右依次是新建下载(New URL)、恢复(Resume)、停止(Stop)、删除(Delete)、设置(Settings)、浏览器集成(Browsers)、关于(About)和退出(Quit)。

![alt 001](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/001.png)


NDM的设置选项也只有三个简单的分栏，通用（General）、代{过}{滤}理（Proxy/Socks）、密码（passwords）。
一般设置以下两项即可：

> 1、最大连接数（Max connections per Download）：为了保证兼容性和稳定性，推荐16线程；
> 
> 2、下载目录（Download Directory）：根据自己的存储磁盘为止设置即可。

![alt 004](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/004.png)


代理服务器选项，NDM支持的代理协议在支持IDM所拥有的传统Socks协议的基础上，增加了HTTP Proxy代{过}{滤}理协议，更加便捷化。

![alt 005](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/005.png)
 
 
IDM的大受欢迎的一个亮点就是拥有丰富的浏览器支持，包含IE, Chrome, AOL, MSN, Mozilla, Netscape, Firefox, Avant Browser等等。
DNM同样支持主流浏览器集成，包括Chrome、Firefox、MicroSoft Edge等主流浏览器，并且集成操作更加简单直观。
&ensp;&ensp;安装插件后可以使用网页资源嗅探功能，以Microsoft Edge作为演示，回到NDM主界面点击浏览器（Browsers）--Microsoft Edge下的Add Edge Extension即可跳转到Edge扩展商店，直接获取即可。

![alt 007](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/007.png)


以新浪微博的视频为例，播放视频后可以在播放器上看到嗅探资源选项，点击后可以看到嗅探到的资源格式和体积大小,相同长度的视频往往体积约到的视频质量越高。

![alt 008](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/008.png)


点击资源即可调用ndm进行下载，在任务弹框中内可以获得文件信息，包括文件大小（File Size）、下载进度（Downloaded）、下载速度（Bandwidth）。底部的Segments：6即为当前使用6个线程进行下载。可以根据需要点击Options进行实时设置。

![alt 009](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/009.png)


下载完成后即可看到下方截图的弹窗提示，点击Open Folder按钮即可打开文件下载目录，点击Open按钮即可打开文件，Close用来关闭弹窗提示。如果不想显示这个下载完成的弹窗提示，可以在主界面设置中取消Show Download Completion Dialog选项的勾即可。

![alt 010](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/010.png)


简单的来跑一个百度网盘文件下载速度的对比，通过下面两张图可以看到NDM和IDM速度相差无几，可见NDM的下载性能完全可以满足日常需要。

 ![alt 011](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/011.png)
 ![alt 012](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/012.png)
 
 
前面提到NDM目前只有英语支持，不过不需要担心这一点，萝卜哥正在积极联系NDM开发者，非常愿意提供中文支持。

![alt 013](https://cdn.staticaly.com/gh/ixmu/Note/master/weixin/ndm/013.png)
  
  
最后来总结一下，NDM是一款免费、轻量、高速的下载工具，支持IDM所支持的全部下载协议。NDM主打特色就是界面简洁和高速下载，没有那么多复杂的设置按钮，对于日常仅仅是作为基本下载工具的人而言，NDM更加适合你。
白嫖党ps：它是免费的，可以随便嫖！！！

官网地址：https://www.neatdownloadmanager.com/


中文尝鲜：https://pan.lanzoui.com/b03ztevle


密码:a233


  [1]: https://raw.fastgit.org/ixmu/Note/master/weixin/ndm/001.png "001"
